import os
import torch
from Beam import *
from translate import *

from torch.autograd import Variable


class Evaluator:

    def __init__(self, dataloader_2d, dataloader_3d, path, cfg, reference_dict):

        self.path = path
        self.cfg = cfg
        self.dataloader_2d = dataloader_2d
        self.dataloader_3d = dataloader_3d
        self.reference_dict = reference_dict



        self.prediction_dict = {}
        self.scores = {}
        self.losses = {}

    def prediction_list(self, model, cfg, args, voc, beam_length):
        self.prediction_dict = {}
        ide_list = []
        caption_list = []
        model.eval()
        with torch.no_grad():
            if cfg.feats_3d == False:
                self.dataloader_3d = self.dataloader_2d
            for (data_2d, data_3d) in zip(self.dataloader_2d, self.dataloader_3d):
                features_2d, targets, _, _, ides = data_2d
                if cfg.feats_3d:
                    features_3d, _, _, _, _ = data_3d
                else:
                    features_3d = 0
                max_len = 20


                targets = targets.transpose(0, 1)

                so = torch.ones((targets.size()[0], 1), dtype=int)

                trg_temp = torch.cat((so, targets), 1)

                img_batch_2d = Variable(features_2d)

                if cfg.feats_3d:
                    img_batch_3d = Variable(features_3d)
                else:
                    img_batch_3d = Variable(torch.Tensor(features_3d))

                img_batch_2d = img_batch_2d.cuda()
                img_batch_3d = img_batch_3d.cuda()

                trg = Variable(trg_temp)
                trg = trg.cuda()

                temp = beam_search(img_batch_2d.float(), img_batch_3d.float(), trg, model, cfg, args, max_len, voc,
                                   beam_length)

                cap_txt = []
                cap_txt.append(temp)
                print(cap_txt)


                ide_list += ides
                caption_list += cap_txt

        for a in zip(ide_list, caption_list):
            self.prediction_dict[str(a[0])] = [a[1].strip()]

    def evaluate(self, scorer, model, epoch, cfg, opt, voc, beam_length, loss=9999):

        self.prediction_list(model, cfg, opt, voc, beam_length)

        scores = scorer.score(self.reference_dict, self.prediction_dict)

        self.scores[epoch] = scores
        self.losses[epoch] = loss

        return scores

    def save_model(self, model, filename):
        print('Better result saving models....')
        path = os.path.join('saved_models', filename)

        torch.save(model, path)