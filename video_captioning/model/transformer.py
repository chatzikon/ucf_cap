"""
 Copyright (c) 2018, salesforce.com, inc.
 All rights reserved.
 SPDX-License-Identifier: BSD-3-Clause
 For full license text, see the LICENSE file in the repo root or https://opensource.org/licenses/BSD-3-Clause
"""

import torch
from torch import nn
from torch.nn import functional as F
from torch.autograd import Variable

# from torchtext import data
# from torchtext import datasets
# from pycrayon import CrayonClient
# from hyperopt import fmin, tpe, hp, STATUS_OK, STATUS_FAIL

import random
import string
import sys
import math
# import spacy
import uuid
import numpy as np

sys.path.insert(0, './tools/densevid_eval/coco-caption') # Hack to allow the import of pycocoeval
from pycocoevalcap.tokenizer.ptbtokenizer import PTBTokenizer

# import contexts

INF = 1e10


def create_masks(trg, cfg):
    if trg is not None:
        trg_mask = (trg != cfg.PAD_token).unsqueeze(-2)
        size = trg.size(1)  # get seq_len for matrix
        np_mask = nopeak_mask(size)
        if trg.is_cuda:
            np_mask.cuda()
        trg_mask = trg_mask & np_mask

    else:
        trg_mask = None
    return trg_mask


def nopeak_mask(size):
    np_mask = np.triu(np.ones((1, size, size)),
                      k=1).astype('uint8')
    np_mask = Variable(torch.from_numpy(np_mask) == 0)
    np_mask = np_mask.cuda()
    return np_mask


def norm11(x):
    x = x / x.sum(0).expand_as(x)
    x[torch.isnan(x)] = 0  # if an entire column is zero, division by 0 will cause NaNs
    x = 2 * x - 1
    return x



def positional_encodings_like(x, t=None):
    if t is None:
        positions = torch.arange(0, x.size(1)).float()
        if x.is_cuda:
           positions = positions.cuda(x.get_device())
    else:
        positions = t
    encodings = torch.zeros(*x.size()[1:])
    if x.is_cuda:
        encodings = encodings.cuda(x.get_device())


    for channel in range(x.size(-1)):
        if channel % 2 == 0:
            encodings[:, channel] = torch.sin(
                positions / 10000 ** (channel / x.size(2)))
        else:
            encodings[:, channel] = torch.cos(
                positions / 10000 ** ((channel - 1) / x.size(2)))
    return Variable(encodings)




def mask(targets, out):
    mask = (targets != 0)
    out_mask = mask.unsqueeze(-1).expand_as(out)

    return targets[mask], out[out_mask].view(-1, out.size(-1))

# torch.matmul can't do (4, 3, 2) @ (4, 2) -> (4, 3)
def matmul(x, y):
    if x.dim() == y.dim():
        return x @ y
    if x.dim() == y.dim() - 1:
        return (x.unsqueeze(-2) @ y).squeeze(-2)
    return (x @ y.unsqueeze(-2)).squeeze(-2)

class LayerNorm(nn.Module):

    def __init__(self, d_model, eps=1e-6):
        super().__init__()
        self.gamma = nn.Parameter(torch.ones(d_model))
        self.beta = nn.Parameter(torch.zeros(d_model))
        self.eps = eps

    def forward(self, x):
        mean = x.mean(-1, keepdim=True)
        std = x.std(-1, keepdim=True)
        return self.gamma * (x - mean) / (std + self.eps) + self.beta

class ResidualBlock(nn.Module):

    def __init__(self, layer, d_model, drop_ratio):
        super().__init__()
        self.layer = layer
        self.dropout = nn.Dropout(drop_ratio)
        self.layernorm = LayerNorm(d_model)

    def forward(self, *x):

        return self.layernorm(x[0] + self.dropout(self.layer(*x)))




        #return x[0] + self.dropout(self.layer(*x))

class Attention(nn.Module):

    def __init__(self, d_key, drop_ratio, causal):
        super().__init__()
        self.scale = math.sqrt(d_key)
        self.dropout = nn.Dropout(drop_ratio)
        self.causal = causal

    def forward(self, query, key, value,mask):
        dot_products = matmul(query, key.transpose(1, 2))
        if query.dim() == 3 and (self is None or self.causal):
            tri = torch.ones(key.size(1), key.size(1)).triu(1) * INF
            if key.is_cuda:
                tri = tri.cuda(key.get_device())

            dot_products.data.sub_(tri.unsqueeze(0))


            if mask is not None:

                #mask = mask.unsqueeze(1)
                dot_products = dot_products.masked_fill(mask == 0, -1e9)

        return matmul(self.dropout(F.softmax(dot_products / self.scale, dim=-1)), value)

class MultiHead(nn.Module):

    def __init__(self, d_key, d_value, n_heads, drop_ratio, causal=False):
        super().__init__()
        self.attention = Attention(d_key, drop_ratio, causal=causal)
        self.wq = nn.Linear(d_key, d_key, bias=False)
        self.wk = nn.Linear(d_key, d_key, bias=False)
        self.wv = nn.Linear(d_value, d_value, bias=False)
        self.wo = nn.Linear(d_value, d_key, bias=False)
        self.n_heads = n_heads

    def forward(self, query, key, value,trg_mask):
        query, key, value = self.wq(query), self.wk(key), self.wv(value)
        query, key, value = (
            x.chunk(self.n_heads, -1) for x in (query, key, value))



        return self.wo(torch.cat([self.attention(q, k, v,trg_mask)
                          for q, k, v in zip(query, key, value)], -1))

class FeedForward(nn.Module):

    def __init__(self, d_model, d_hidden):
        super().__init__()
        self.linear1 = nn.Linear(d_model, d_hidden)
        self.linear2 = nn.Linear(d_hidden, d_model)

    def forward(self, x):
        return self.linear2(F.relu(self.linear1(x)))

class EncoderLayer(nn.Module):

    def __init__(self, d_model, d_hidden, n_heads, drop_ratio):
        super().__init__()
        self.layernorm = LayerNorm(d_model)
        self.selfattn = ResidualBlock(
            MultiHead(d_model, d_model, n_heads, drop_ratio),
            d_model, drop_ratio)
        self.feedforward = ResidualBlock(FeedForward(d_model, d_hidden),
                                         d_model, drop_ratio)

    def forward(self, x):
        #return  self.layernorm(out)
        return self.feedforward(self.selfattn(x, x, x, None))

class DecoderLayer(nn.Module):

    def __init__(self, d_model, d_hidden, n_heads, drop_ratio,attention):
        super().__init__()
        self.selfattn = ResidualBlock(
            MultiHead(d_model, d_model, n_heads, drop_ratio, causal=True),
            d_model, drop_ratio)

        if attention == 'tvm_add' or attention == 'tvm_att':

            self.selfattn_2d = ResidualBlock(
                MultiHead(d_model, d_model, n_heads, drop_ratio),
                d_model, drop_ratio)

            self.selfattn_3d = ResidualBlock(
                MultiHead(d_model, d_model, n_heads, drop_ratio),
                d_model, drop_ratio)
        else:
            self.attention = ResidualBlock(
                MultiHead(d_model, d_model, n_heads, drop_ratio),
                d_model, drop_ratio)

        if  attention == 'tvm_att':
            self.linear_down=nn.Linear(d_model*3, d_model)
            self.selfattn_fuse = ResidualBlock(
                MultiHead(d_model, d_model, n_heads, drop_ratio),
                d_model, drop_ratio)


        self.feedforward = ResidualBlock(FeedForward(d_model, d_hidden),
                                         d_model, drop_ratio)



    def forward(self, x, encoding,trg_mask,attention):
        x = self.selfattn(x, x, x,trg_mask)
        if attention=='tvm_add':

            temp_a=self.selfattn_2d(x, encoding[0], encoding[0], None)
            temp_b = self.selfattn_3d(x, encoding[1], encoding[1], None)
            return self.feedforward(temp_a+temp_b)
        elif attention=='tvm_att':
            temp_a = self.selfattn_2d(x, encoding[0], encoding[0], None)
            temp_b = self.selfattn_3d(x, encoding[1], encoding[1], None)
            temp=torch.cat((temp_a,x,temp_b),2)
            temp_d=self.linear_down(temp)



            return self.feedforward(self.selfattn_fuse(x, temp_d,temp_d, None))
        else:
            return self.feedforward(self.attention(x, encoding, encoding,None))

class Encoder(nn.Module):

    def __init__(self, d_model, d_hidden, n_vocab, n_layers, n_heads,
                 drop_ratio):
        super().__init__()

        self.d_model=d_model




        self.layers = nn.ModuleList(
            [EncoderLayer(d_model, d_hidden, n_heads, drop_ratio)
             for i in range(n_layers)])
        self.dropout = nn.Dropout(drop_ratio)

    def forward(self, x, mask=None):
        # x = self.linear(x)

        # if self.d_model==2048:
        #     x=self.norm(x)



        x = x+positional_encodings_like(x)
        x = self.dropout(x)
        if mask is not None:
            x = x*mask
        encoding = []
        for layer in self.layers:
            x = layer(x)
            if mask is not None:
                x = x*mask
            encoding.append(x)

        return encoding

class Decoder(nn.Module):

    def __init__(self, d_model, d_hidden, vocab, n_layers, n_heads,
                 drop_ratio,attention):
        super().__init__()







        self.layers = nn.ModuleList(
            [DecoderLayer(d_model, d_hidden, n_heads, drop_ratio,attention)
             for i in range(n_layers)])
        self.dropout = nn.Dropout(drop_ratio)
        self.d_model = d_model
        self.vocab = vocab
        self.d_out = vocab.num_words


    def forward(self, x, encoding,trg_mask,out_layer,attention):



        x = F.embedding(x, out_layer.weight * math.sqrt(self.d_model))


        #x=self.embedding(x)* math.sqrt(self.d_model)




        x = x+positional_encodings_like(x)
        x = self.dropout(x)

        if attention=='tvm_add' or attention=='tvm_att':
            for layer, enc_2d,enc_3d in zip(self.layers, encoding[0],encoding[1]):
                enc=[enc_2d,enc_3d]
                x = layer(x, enc,trg_mask,attention)
        else:
            for layer, enc in zip(self.layers, encoding):
                x = layer(x, enc,trg_mask,attention)





        return x

    def greedy(self, encoding, T):
        B, _, H = encoding[0].size()
        # change T to 20, max # of words in a sentence
        # T = 40
        # T *= 2
        prediction = Variable(encoding[0].data.new(B, T).long().fill_(
            self.vocab.cfg.PAD_token))
        hiddens = [Variable(encoding[0].data.new(B, T, H).zero_())
                   for l in range(len(self.layers) + 1)]
        embedW = self.out.weight * math.sqrt(self.d_model)
        hiddens[0] = hiddens[0] + positional_encodings_like(hiddens[0])
        for t in range(T):
            if t == 0:
                hiddens[0][:, t] = hiddens[0][:, t] + F.embedding(Variable(
                    encoding[0].data.new(B).long().fill_(
                        self.vocab.cfg.SOS_token)), embedW)
            else:
                hiddens[0][:, t] = hiddens[0][:, t] + F.embedding(prediction[:, t - 1],
                                                                embedW)
            hiddens[0][:, t] = self.dropout(hiddens[0][:, t])
            for l in range(len(self.layers)):
                x = hiddens[l][:, :t + 1]
                x = self.layers[l].selfattn(hiddens[l][:, t], x, x)
                hiddens[l + 1][:, t] = self.layers[l].feedforward(
                    self.layers[l].attention(x, encoding[l], encoding[l]))

            _, prediction[:, t] = self.out(hiddens[-1][:, t]).max(-1)
        return hiddens, prediction


    def sampling(self, encoding, gt_token, T, sample_prob, trg_mask,is_argmax=True):
        B, _, H = encoding[0].size()
        # change T to 20, max # of words in a sentence
        # T = 40
        # T *= 2

        prediction = Variable(encoding[0].data.new(B, T).long().fill_(
            self.vocab.cfg.PAD_token))
        hiddens = [Variable(encoding[0].data.new(B, T, H).zero_())
                   for _ in range(len(self.layers) + 1)]
        embedW = self.out.weight * math.sqrt(self.d_model)
        hiddens[0] = hiddens[0] + positional_encodings_like(hiddens[0])
        for t in range(T):
            if t == 0:
                hiddens[0][:, t] = hiddens[0][:, t] + F.embedding(Variable(
                    encoding[0].data.new(B).long().fill_(
                        self.vocab.cfg.SOS_token)), embedW)
            else:
                use_model_pred = np.random.binomial(1, sample_prob, 1)[0]
                if use_model_pred > 0:
                    hiddens[0][:, t] = hiddens[0][:, t] + F.embedding(
                        prediction[:, t - 1],
                        embedW)
                else:
                    hiddens[0][:, t] = hiddens[0][:, t] + F.embedding(
                        gt_token[:, t], # t since gt_token start with init
                        embedW)
            hiddens[0][:, t] = self.dropout(hiddens[0][:, t])
            for l in range(len(self.layers)):
                x = hiddens[l][:, :t + 1]
                x = self.layers[l].selfattn(hiddens[l][:, t], x, x,trg_mask)
                hiddens[l + 1][:, t] = self.layers[l].feedforward(
                    self.layers[l].attention(x, encoding[l], encoding[l],trg_mask))

            if is_argmax:
                _, prediction[:, t] = self.out(hiddens[-1][:, t]).max(-1)
            else:
                pred_prob = F.softmax(self.out(hiddens[-1][:, t]), dim=-1)
                prediction[:, t] = torch.multinomial(pred_prob,
                                                        num_samples=1,
                                                        replacement=True)
                prediction[:, t].detach_()

        return prediction




class RealTransformer(nn.Module):

    def __init__(self, d_model, n_vocab_src, vocab_trg, fusion,attention,d_hidden=2048,
                 n_layers=6, n_heads=8, drop_ratio=0.1):
        super().__init__()
        self.encoder = Encoder(d_model, d_hidden, n_vocab_src, n_layers,
                                n_heads, drop_ratio)


        if fusion=='intermediate' :
            self.encoder_3d = Encoder(d_model, d_hidden, n_vocab_src, n_layers,
                                   n_heads, drop_ratio)



            if  attention=='both' or attention=='both_add':
                self.selfattn_2d = ResidualBlock(
                    MultiHead(d_model, d_model, n_heads, drop_ratio),
                    d_model, drop_ratio)

                self.selfattn_3d = ResidualBlock(
                    MultiHead(d_model, d_model, n_heads, drop_ratio),
                    d_model, drop_ratio)
            elif attention=='2d':
                self.selfattn_2d = ResidualBlock(
                    MultiHead(d_model, d_model, n_heads, drop_ratio),
                    d_model, drop_ratio)
            elif attention == '3d':
                self.selfattn_3d = ResidualBlock(
                    MultiHead(d_model, d_model, n_heads, drop_ratio),
                    d_model, drop_ratio)


            if attention=='both' or attention=='concat':
                self.fuse_out=nn.Linear(d_hidden,d_model)

        self.decoder = Decoder(d_model, d_hidden, vocab_trg, n_layers,
                              n_heads, drop_ratio,attention)

        self.n_layers = n_layers
        self.tokenizer = PTBTokenizer()
        self.fusion=fusion
        self.attention=attention

    def denum(self, data):
        return ' '.join(self.decoder.vocab.itos[i] for i in data).replace(
            ' <eos>', '').replace(' <pad>', '').replace(' .', '').replace('  ', '')

    def forward(self, x,x_3d, s, cfg,out_layer,x_mask=None, sample_prob=0):


        encoding = self.encoder(x, x_mask)



        if self.fusion=='intermediate' and cfg.feats_3d==True:
            encoding_3d = self.encoder_3d(x_3d, x_mask)





            if self.attention=='concat':
                encoding_f=[]
                for i in range(len(encoding)):
                    encoding_f.append(torch.cat((encoding[i], encoding_3d[i]), 2))

                encoding=encoding_f
            elif self.attention=='add':
                encoding = encoding+encoding_3d
            elif self.attention == 'both' or self.attention == 'both_add':
                h1=[]
                h2=[]
                for i in range(len(encoding)):
                    h1.append(self.selfattn_2d(encoding_3d[i],encoding[i], encoding[i], None))
                    h2.append(self.selfattn_3d(encoding[i], encoding_3d[i], encoding_3d[i], None))

                if self.attention == 'both':
                    encoding_f = []
                    for i in range(len(encoding)):
                        encoding_f.append(torch.cat((h1[i],h2[i]), 2))
                    encoding = encoding_f

                elif self.attention == 'both_add':
                    encoding = h1 + h2

            elif self.attention == '2d':
                encoding_f=[]
                for i in range(len(encoding)):
                    encoding_f.append(self.selfattn_2d(encoding_3d[i], encoding[i], encoding[i], None))
                encoding=encoding_f

            elif self.attention == '3d':
                encoding_f = []
                for i in range(len(encoding)):
                    encoding_f.append(self.selfattn_3d(encoding[i], encoding_3d[i], encoding_3d[i], None))
                encoding=encoding_f
            elif self.attention == 'tvm_add' or self.attention == 'tvm_att':
                encoding_f=[encoding,encoding_3d]

                encoding=encoding_f



            if self.attention=='both' or self.attention=='concat':
                temp=[]
                for i in range(len(encoding)):
                    temp.append(self.fuse_out(encoding[i]))
                encoding=temp




        #max_sent_len = 20
        if not self.training:
            # if isinstance(s, list):
            #     print(ent)
            #     hiddens, _ = self.decoder.greedy(encoding, max_sent_len)
            #     h = hiddens[-1]
            #     targets = None
            # else:
                sd = s[s != 2].view(s.size()[0], s.size()[1] - 1)
                #trg_mask = create_masks(sd, cfg)
                trg_mask=None

                h = self.decoder(sd.contiguous(), encoding,trg_mask,out_layer, self.attention)




        else:

            if sample_prob == 0:




                sd=s[s != 2].view(s.size()[0],s.size()[1]-1)


                #trg_mask = create_masks(sd,cfg)
                trg_mask = None



                h = self.decoder(sd.contiguous(), encoding,trg_mask,out_layer, self.attention)












            else:



                sd = s[s != 2].view(s.size()[0], s.size()[1] - 1)

                #trg_mask = create_masks(sd, cfg)
                trg_mask = None

                model_pred = self.decoder.sampling(encoding, sd,
                                                   s.size(1) - 2,
                                                   sample_prob,trg_mask,
                                                   is_argmax=True)




                model_pred.detach()
                #

                new_y = torch.cat((
                    Variable(model_pred.data.new(s.size(0), 1).long().fill_(
                        self.decoder.vocab.cfg.SOS_token)),
                    model_pred), 1)




                h = self.decoder(new_y, encoding,trg_mask,True,out_layer, self.attention)




        # temp=s[:, 1:].contiguous()
        # b=temp.view( temp.size(-1)*temp.size(-2))
        # a=logits.view(-1, logits.size(-1))

        return h

    def greedy(self, x, x_mask, T):
        encoding = self.encoder(x, x_mask)

        _, pred = self.decoder.greedy(encoding, T)
        sent_lst = []
        for i in range(pred.data.size(0)):
            sent_lst.append(self.denum(pred.data[i]))
        return sent_lst







