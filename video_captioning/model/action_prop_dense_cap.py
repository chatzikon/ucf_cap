"""
 Copyright (c) 2018, salesforce.com, inc.
 All rights reserved.
 SPDX-License-Identifier: BSD-3-Clause
 For full license text, see the LICENSE file in the repo root or https://opensource.org/licenses/BSD-3-Clause
"""


from torch import nn
from torch.autograd import Variable
from .transformer import  RealTransformer
import torch
import numpy as np
import torch.nn.functional as F
import math
import time

INF = 1e10


def mask(targets, out):
    mask = (targets != 0)
    out_mask = mask.unsqueeze(-1).expand_as(out)

    return targets[mask], out[out_mask].view(-1, out.size(-1))



def matmul(x, y):
    if x.dim() == y.dim():
        return x @ y
    if x.dim() == y.dim() - 1:
        return (x.unsqueeze(-2) @ y).squeeze(-2)
    return (x @ y.unsqueeze(-2)).squeeze(-2)


class LayerNorm(nn.Module):

    def __init__(self, d_model, eps=1e-6):
        super().__init__()
        self.gamma = nn.Parameter(torch.ones(d_model))
        self.beta = nn.Parameter(torch.zeros(d_model))
        self.eps = eps

    def forward(self, x):
        mean = x.mean(-1, keepdim=True)
        std = x.std(-1, keepdim=True)
        return self.gamma * (x - mean) / (std + self.eps) + self.beta

class ResidualBlock(nn.Module):

    def __init__(self, layer, d_model, drop_ratio):
        super().__init__()
        self.layer = layer
        self.dropout = nn.Dropout(drop_ratio)
        self.layernorm = LayerNorm(d_model)

    def forward(self, *x):
        return self.layernorm(x[0] + self.dropout(self.layer(*x)))


class Attention(nn.Module):

    def __init__(self, d_key, drop_ratio, causal):
        super().__init__()
        self.scale = math.sqrt(d_key)
        self.dropout = nn.Dropout(drop_ratio)
        self.causal = causal

    def forward(self, query, key, value,mask):
        dot_products = matmul(query, key.transpose(1, 2))
        if query.dim() == 3 and (self is None or self.causal):
            tri = torch.ones(key.size(1), key.size(1)).triu(1) * INF
            if key.is_cuda:
                tri = tri.cuda(key.get_device())
            dot_products.data.sub_(tri.unsqueeze(0))

            if mask is not None:

                #mask = mask.unsqueeze(1)
                dot_products = dot_products.masked_fill(mask == 0, -1e9)

        return matmul(self.dropout(F.softmax(dot_products / self.scale, dim=-1)), value)


class MultiHead(nn.Module):

    def __init__(self, d_key, d_value, n_heads, drop_ratio, causal=False):
        super().__init__()
        self.attention = Attention(d_key, drop_ratio, causal=causal)
        self.wq = nn.Linear(d_key, d_key, bias=False)
        self.wk = nn.Linear(d_key, d_key, bias=False)
        self.wv = nn.Linear(d_value, d_value, bias=False)
        self.wo = nn.Linear(d_value, d_key, bias=False)
        self.n_heads = n_heads

    def forward(self, query, key, value,trg_mask):
        query, key, value = self.wq(query), self.wk(key), self.wv(value)
        query, key, value = (
            x.chunk(self.n_heads, -1) for x in (query, key, value))
        return self.wo(torch.cat([self.attention(q, k, v,trg_mask)
                          for q, k, v in zip(query, key, value)], -1))


class DropoutTime1D(nn.Module):
    '''
        assumes the first dimension is batch, 
        input in shape B x T x H
        '''
    def __init__(self, p_drop):
        super(DropoutTime1D, self).__init__()
        self.p_drop = p_drop

    def forward(self, x):
        if self.training:
            mask = x.data.new(x.data.size(0),x.data.size(1), 1).uniform_()
            mask = Variable((mask > self.p_drop).float())
            return x * mask
        else:
            return x * (1-self.p_drop)

    def init_params(self):
        pass

    def __repr__(self):
        repstr = self.__class__.__name__ + ' (\n'
        repstr += "{:.2f}".format(self.p_drop)
        repstr += ')'
        return repstr


class ActionPropDenseCap(nn.Module):
    def __init__(self, d_model, d_hidden, n_layers, n_heads, vocab,
                 in_emb_dropout,
                 cap_dropout, cfg, downsample, fusion,attention,
                 learn_mask=False):
        super(ActionPropDenseCap, self).__init__()

        self.learn_mask = learn_mask
        self.d_model = d_model
        self.downsample = downsample
        self.fusion=fusion
        self.attention=attention


        # self.mask_model = nn.Sequential(
        #     nn.Linear(d_model+window_length, d_model, bias=False),
        #     nn.BatchNorm1d(d_model),
        #     nn.ReLU(),
        #     nn.Linear(d_model, window_length),
        # )


        if fusion=='early' and cfg.feats_3d:
            if  downsample or attention=='2d' or attention=='3d' or attention=='both_add' or attention=='add' :

                self.emb_2d = nn.Linear(cfg.input_size, d_model)
                self.emb_3d = nn.Linear(2 * 2048, d_model)

            else:
                self.emb_2d = nn.Linear(cfg.input_size, d_model // 2)
                self.emb_3d = nn.Linear(2 * 2048, d_model // 2)

            if downsample:
                if attention == 'both' or attention == 'concat':
                    self.down = nn.Linear(d_model * 2, d_model)

            if (downsample and attention=='both') or attention=='both_add' :

                self.selfattn_2d = ResidualBlock(
                    MultiHead(d_model, d_model, n_heads, cap_dropout),
                    d_model, cap_dropout)

                self.selfattn_3d = ResidualBlock(
                    MultiHead(d_model, d_model, n_heads, cap_dropout),
                    d_model, cap_dropout)


            elif attention=='both':

                self.selfattn_2d = ResidualBlock(
                    MultiHead(d_model//2, d_model//2, n_heads, cap_dropout),
                    d_model//2, cap_dropout)

                self.selfattn_3d = ResidualBlock(
                    MultiHead(d_model//2, d_model//2, n_heads, cap_dropout),
                    d_model//2, cap_dropout)


            elif attention=='2d':

                self.selfattn_2d = ResidualBlock(
                    MultiHead(d_model, d_model, n_heads, cap_dropout),
                    d_model, cap_dropout)

            elif attention == '3d':

                self.selfattn_3d = ResidualBlock(
                    MultiHead(d_model, d_model, n_heads, cap_dropout),
                    d_model, cap_dropout)



        else:
            #if self.d_model != 2048:
            self.emb_2d = nn.Linear(cfg.input_size, d_model)

        self.emb_out = nn.Sequential(
            # nn.BatchNorm1d(h_dim),
            DropoutTime1D(in_emb_dropout),
            nn.ReLU()
        )



        #self.flow_emb = nn.Linear(1024, d_model // 2)


        if fusion=='intermediate' and cfg.feats_3d:
            self.emb_3d = nn.Linear(2*2048, d_model)




        if fusion=='late' and cfg.feats_3d:
            if cfg.motion == 'resnext101':
                self.emb_3d = nn.Linear(2048, d_model)
            else:
                self.emb_3d = nn.Linear(2*2048, d_model)
            self.cap_model_3d = RealTransformer(d_model,0,  vocab, fusion,None,
            d_hidden = d_hidden,
            n_layers = n_layers,
            n_heads = n_heads,
            drop_ratio = cap_dropout)

            if attention=='both' or attention=='both_add' :

                self.selfattn_2d = ResidualBlock(
                    MultiHead(d_model, d_model, n_heads, cap_dropout),
                    d_model, cap_dropout)

                self.selfattn_3d = ResidualBlock(
                    MultiHead(d_model, d_model, n_heads, cap_dropout),
                    d_model, cap_dropout)

                if attention == 'both':

                    self.out1 = nn.Linear(2 * d_model, d_model)


            elif attention=='2d':

                self.selfattn_2d = ResidualBlock(
                    MultiHead(d_model, d_model, n_heads, cap_dropout),
                    d_model, cap_dropout)

            elif attention == '3d':

                self.selfattn_3d = ResidualBlock(
                    MultiHead(d_model, d_model, n_heads, cap_dropout),
                    d_model, cap_dropout)

            elif attention=='concat':
                self.out1 = nn.Linear(2 * d_model, d_model)


        self.cap_model = RealTransformer(d_model,
                                     0, #share the encoder
                                     vocab,fusion,attention,
                                     d_hidden=d_hidden,
                                     n_layers=n_layers,
                                     n_heads=n_heads,
                                     drop_ratio=cap_dropout)


        self.bce_loss = nn.BCEWithLogitsLoss()


        self.out = nn.Linear(d_model, vocab.num_words)


    def forward(self, x_2d,x_3d, sentence,cfg,
                sample_prob=0):








        if self.fusion=='early' and cfg.feats_3d==True:
            input_2d = self.emb_2d(x_2d.contiguous())
            input_3d = self.emb_3d(x_3d.contiguous())



            if self.attention=='concat':
                input=torch.cat((input_2d,input_3d),2)

            elif self.attention=='add':

                input=input_2d+input_3d

            elif self.attention=='both' or  self.attention=='both_add':


                h1 = self.selfattn_2d(input_3d, input_2d, input_2d, None)
                h2 = self.selfattn_3d(input_2d, input_3d, input_3d, None)

                if self.attention=='both':
                    input = torch.cat((h1, h2), 2)
                elif self.attention=='both_add':
                    input=h1+h2

            elif self.attention == '2d':
                input = self.selfattn_2d(input_3d, input_2d, input_2d, None)

            elif self.attention == '3d':
                input = self.selfattn_3d(input_2d, input_3d, input_3d, None)

            if self.downsample:
                input = self.down(input)


        else:
            input = self.emb_2d(x_2d.contiguous())



        x_emb = self.emb_out(input)


        if self.fusion=='intermediate' and cfg.feats_3d==True:
            input_3d = self.emb_3d(x_3d.contiguous())
            x_emb_3d = self.emb_out(input_3d)
            x3_emb=x_emb_3d
        else:
            x3_emb =0



        h = self.cap_model(x_emb, x3_emb, sentence, cfg, self.out,None,sample_prob=sample_prob)





        if self.fusion=='late' and cfg.feats_3d==True:
            input_3d = self.emb_3d(x_3d.contiguous())
            x_emb_3d = self.emb_out(input_3d)

            h_3d = self.cap_model_3d(x_emb_3d,0, sentence, cfg, self.out, None, sample_prob=sample_prob)


            if self.attention=='both':


                h1=self.selfattn_2d(h_3d,h,h,None)
                h2=self.selfattn_3d(h,h_3d,h_3d,None)
                h=torch.cat((h1,h2),2)

                h = self.out1(h)

            elif self.attention=='both_add':

                h1 = self.selfattn_2d(h_3d, h, h, None)
                h2 = self.selfattn_3d(h, h_3d, h_3d, None)
                h=h1+h2


            elif self.attention=='2d':
                h = self.selfattn_2d(h_3d, h, h, None)

            elif self.attention == '3d':
                h = self.selfattn_3d(h, h_3d, h_3d, None)
            elif self.attention == 'concat':
                h = torch.cat((h, h_3d), 2)
                h = self.out1(h)
            elif self.attention == 'add':
                h = h+h_3d




        targets, h = mask(sentence[:, 1:].contiguous(), h)



        logits = self.out(h)



        return (logits,targets)



