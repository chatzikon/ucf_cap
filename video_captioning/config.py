

import torch
import os


class ConfigTRF_fc:
    '''
    Hyperparameter settings for Mean Pooling model.
    '''
    def __init__(self, model_name='trf', r3d=False):
        self.model_name = model_name
        self.decoder_type = 'lstm'
        self.feat_len = 50
        self.feat_len_3d=50
        self.max_target_len=20
        if torch.cuda.is_available():
            self.device = torch.device('cuda') # In case of multiple GPU system use 'cuda:x' where x is GPU device id
        else:
            self.device = torch.device('cpu')

        self.batch_size = 40 #suitable
        self.val_batch_size = 10

        self.input_size = 2048

        self.videofeat_size = 512
        self.hidden_size = 512
        self.n_layers = 1
        self.dropout = 0.1


        self.encoder_lr = 1e-4
        self.decoder_lr = 1e-4
        self.teacher_forcing_ratio = 1.0 #
        self.clip = 5 # clip the gradient to counter exploding gradient problem
        self.print_every = 400

        self.feats_3d=r3d

        self.SOS_token = 1
        self.EOS_token = 2
        self.PAD_token = 0
        self.UNK_token = 3




class Path:

    def __init__(self,cfg,working_path):


        self.local_path = os.path.join(working_path,'UCF_V2')

        self.caption_path = os.path.join(self.local_path,'captions')

        self.name_mapping_file = os.path.join(self.caption_path,'youtube_mapping.txt')
        self.train_annotation_file = os.path.join(self.caption_path,'sents_train_lc_nopunc.txt')
        self.val_annotation_file = os.path.join(self.caption_path,'sents_val_lc_nopunc.txt')
        self.test_annotation_file = os.path.join(self.caption_path,'sents_test_lc_nopunc.txt')


        self.feature_path = os.path.join(self.local_path,'features')
        self.feature_file = os.path.join(self.feature_path,'ucf_resnet152.hdf5')




        self.feature_path_3d = os.path.join(self.local_path, 'features')
        self.feature_file_3d = os.path.join(self.feature_path_3d,'ucf_c3d.hdf5')


        self.prediction_path = 'results'
        self.saved_models_path = 'Saved'



