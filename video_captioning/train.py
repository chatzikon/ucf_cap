import os
import errno
import argparse
import numpy as np
import random
import time


from utils import Utils


# torch
import torch
import torch.nn.functional as F
from torch.autograd import Variable
import torch.optim as optim
from torch.optim import lr_scheduler
from torch.utils.data import DataLoader
from torch.nn.utils import clip_grad_norm_
import torch.distributed as dist
import torch.utils.data.distributed


from evaluate import Evaluator
from config import Path,ConfigTRF_fc
from dictionary import Vocabulary
from datai import DataHandler

# misc
from model.action_prop_dense_cap import ActionPropDenseCap

parser = argparse.ArgumentParser()

# Data input settings

# Model settings: General

parser.add_argument('--n_heads', default=8, type=int)

parser.add_argument('--in_emb_dropout', default=0.1, type=float)
parser.add_argument('--cap_dropout', default=0.2, type=float)



parser.add_argument('--n_layers', default=7, type=int, help='number of layers in the sequence model')

# Optimization: General
parser.add_argument('--cls_weight', default=1.0, type=float)
parser.add_argument('--reg_weight', default=10, type=float)
parser.add_argument('--sent_weight', default=0.25, type=float)
parser.add_argument('--scst_weight', default=0.0, type=float)
parser.add_argument('--mask_weight', default=0.0, type=float)
parser.add_argument('--gated_mask', action='store_true', dest='gated_mask')

# Optimization
parser.add_argument('--optim',default='sgd', help='what update to use? rmsprop|sgd|sgdmom|adagrad|adam')
parser.add_argument('--alpha', default=0.95, type=float, help='alpha for adagrad/rmsprop/momentum/adam')
parser.add_argument('--beta', default=0.999, type=float, help='beta used for adam')
parser.add_argument('--epsilon', default=1e-8, help='epsilon that goes into denominator for smoothing')
parser.add_argument('--loss_alpha_r', default=2, type=int, help='The weight for regression loss')
parser.add_argument('--patience_epoch', default=1, type=int, help='Epoch to wait to determine a pateau')

parser.add_argument('--reduce_factor', default=0.5, type=float, help='Factor of learning rate reduction')




# Evaluation/Checkpointing
parser.add_argument('--save_checkpoint_every', default=1, type=int, help='how many epochs to save a model checkpoint?')
parser.add_argument('--checkpoint_path', default='./saved_models', help='folder to save checkpoints into (empty = this folder)')
parser.add_argument('--losses_log_every', default=1, type=int, help='How often do we snapshot losses, for inclusion in the progress dump? (0 = disable)')
parser.add_argument('--enable_visdom', action='store_true', dest='enable_visdom')


parser.set_defaults(save_train_samplelist=False,
                    load_train_samplelist=False,
                    save_valid_samplelist=False,
                    load_valid_samplelist=False,
                    gated_mask=False,
                    enable_visdom=False)

args = parser.parse_args()







def optimizer_to_cpu(optim):
    for param in optim.state.values():
        # Not sure there are any global tensors in the state dict
        if isinstance(param, torch.Tensor):
            param.data = param.data.cpu()
            if param._grad is not None:
                param._grad.data = param._grad.data.cpu()
        elif isinstance(param, dict):
            for subparam in param.values():
                if isinstance(subparam, torch.Tensor):
                    subparam.data = subparam.data.cpu()
                    if subparam._grad is not None:
                        subparam._grad.data = subparam._grad.data.cpu()



def save_model(model,optimizer,filename,filename_prev,epoch):
        print('Better result saving models....')
        if epoch>0:
            os.remove(os.path.join('saved_models',filename_prev))
        path=os.path.join('saved_models',filename)

        torch.save({'state_dict': model.state_dict(),'optimizer': optimizer.state_dict()},path)




def get_model(vocab, args,cfg,fusion,attention,downsample):
    sent_vocab = vocab
    model = ActionPropDenseCap(d_model=args.d_model,
                               d_hidden=args.d_hidden,
                               n_layers=args.n_layers,
                               n_heads=args.n_heads,
                               vocab=sent_vocab,
                               in_emb_dropout=args.in_emb_dropout,
                               cap_dropout=args.cap_dropout,
                               cfg=cfg, downsample=downsample,
                               fusion=fusion,
                               attention=attention,
                               learn_mask=args.mask_weight > 0,
                               )


    return model



def main(args,batch_size,sample_prob,beam_length,nlayers,dmod,hidden_sz,grad_norm,patience,epochs,feats_3d,sum_index,
         seed,feat_len,learning_rate,fusion,attention, downsample, finetune,path_ft, evaluation,path_ev):
    try:
        os.makedirs(args.checkpoint_path)
    except OSError as e:
        if e.errno == errno.EEXIST:
            print('Directory already exists.')
        else:
            raise





    args.grad_norm=grad_norm
    args.learning_rate=learning_rate




    torch.manual_seed(seed)
    np.random.seed(seed)
    random.seed(seed)

    torch.cuda.manual_seed_all(seed)




    args.patience_epoch=patience
    args.max_epochs=epochs


    args.cuda=True

    args.d_hidden=hidden_sz

    args.sample_prob=sample_prob

    utils = Utils()
    utils.set_seed(seed)

    print('loading dataset')

    cfg = ConfigTRF_fc(r3d=feats_3d)
    cfg.feat_len=feat_len



    local_path = '../datasets/'
    path = Path(cfg, local_path)



    #voc = Vocabulary(cfg)
    # If vocabulary is already saved or downloaded the saved file
    #voc.load()  # comment this if using vocabulary for the first time or with no saved file

    # # Uncomment this block if using vocabulary for the first time or if there is no saved file
    text_dict = {}
    voc = Vocabulary(cfg)
    data_handler = DataHandler(cfg,path,voc)

    text_dict.update(data_handler.train_dict)
    text_dict.update(data_handler.val_dict)
    text_dict.update(data_handler.test_dict)
    for k,v in text_dict.items():
        for anno in v:
            voc.addSentence(anno)
    voc.save()



    print('Vocabulary Size : ', voc.num_words)

    # Datasets and dataloaders
    cfg.batch_size=batch_size

    args.n_layers=nlayers



    args.d_model=dmod







    data_handler = DataHandler(cfg, path, voc)







    train_dset, val_dset, test_dset = data_handler.getDatasets()



    if cfg.feats_3d==True :
        train_loader_2d, val_loader_2d, test_loader_2d, train_loader_3d, val_loader_3d, test_loader_3d = data_handler.getDataloader_3d(train_dset, val_dset, test_dset)
    else:
        train_loader_2d, val_loader_2d, test_loader_2d = data_handler.getDataloader(train_dset, val_dset, test_dset)
        train_loader_3d=0
        val_loader_3d=0
        test_loader_3d=0





    val_evaluator = Evaluator(val_loader_2d,val_loader_3d, path, cfg, data_handler.val_dict)

    test_evaluator = Evaluator(test_loader_2d,test_loader_3d, path, cfg, data_handler.test_dict)




    print(args.cuda)
    print('building model')
    model = get_model(voc, args,cfg,fusion,attention, downsample)



    args.optim = 'sgd'
    if args.optim == 'adam':
        optimizer = optim.Adam(
            filter(lambda p: p.requires_grad, model.parameters()),
            args.learning_rate, betas=(args.alpha, args.beta), eps=args.epsilon)
    elif args.optim == 'sgd':
        optimizer = optim.SGD(
            filter(lambda p: p.requires_grad, model.parameters()),
            args.learning_rate,
            weight_decay=1e-5,
            momentum=args.alpha,
            nesterov=True
        )
    else:
        raise NotImplementedError









    if finetune:
        model_dict = model.state_dict()
        pretrained_dict = torch.load(path_ft)['state_dict']
        filtered_dict = {k: v for k, v in pretrained_dict.items() if k in model_dict}
        model_dict.update(filtered_dict)
        model.load_state_dict(model_dict)



    if args.cuda:
            model.cuda()




    if evaluation:
        train_epoch=0
        epoch_loss=0




        model.load_state_dict(torch.load(path_ev)['state_dict'])
        optimizer.load_state_dict(torch.load(path_ev)['optimizer'])




        scores=test_evaluator.evaluate(utils, model, train_epoch, cfg,args,voc, beam_length,epoch_loss)

        filename = 'test_' + str(0) + '_'  + str(args.learning_rate) + '_loss_' + str(
            0) + '.pt'

        metric_filename = ' bleu4= ' + str(round(scores['Bleu_4'], 4)) + ' METEOR= ' + str(
            round(scores['METEOR'], 4)) + ' ROUGE_L= ' + str(round(scores['ROUGE_L'], 4)) + ' CIDEr= ' + str(
            round(scores['CIDEr'], 4)) + str(args.n_layers) + str(args.d_model)


        print('eval scores')
        print(scores)

        save_model(model, optimizer, filename + metric_filename,'',0)

        return




    # learning rate decay every 1 epoch
    scheduler = lr_scheduler.ReduceLROnPlateau(optimizer, factor=args.reduce_factor,
                                               patience=args.patience_epoch,
                                               verbose=True)

    # Number of parameter blocks in the network
    print("# of param blocks: {}".format(str(len(list(model.parameters())))))

    best_loss = float('inf')
    best_sum=float(-1)


    print(model)

    print('EPOCHS')
    print(args.max_epochs)

    for train_epoch in range(args.max_epochs):
        t_epoch_start = time.time()
        print('Epoch: {}'.format(train_epoch))



        print('train')
        epoch_loss = train(train_epoch, model, optimizer, train_loader_2d,train_loader_3d,args,cfg)
        #
        #
        #
        #
        #
        print('val')
        (valid_loss) = valid(model, val_loader_2d,val_loader_3d,cfg)

        #
        #
        print('eval')
        scores=val_evaluator.evaluate(utils, model, train_epoch, cfg,args,voc,beam_length,epoch_loss)
        valid_sum=scores['Bleu_4']+scores['METEOR']+scores['ROUGE_L']+scores['CIDEr']


        print('eval scores')
        print(scores)





        best_model=valid_loss < best_loss

        if train_epoch==0:
            filename=''
            metric_filename=''
            filename_s=''
            metric_filename_s=''
        if best_model:
            filename_previous=filename
            filename = 'epoch_' + str(train_epoch) +  '_' + str(args.learning_rate) + '_loss_' + str(
                valid_loss) + '.pt'

            metric_filename_previous=metric_filename
            metric_filename=' bleu4= '+str(round(scores['Bleu_4'],4))+' METEOR= '+str(round(scores['METEOR'],4))+' ROUGE_L= '+str(round(scores['ROUGE_L'],4))+' CIDEr= '+str(round(scores['CIDEr'],4))+str(args.n_layers)+str(args.d_model)
            save_model(model,optimizer,filename+metric_filename,filename_previous+metric_filename_previous,train_epoch)

            best_loss = valid_loss
            best_filename=filename+metric_filename
            best_epoch=train_epoch
            print('*'*5)
            print('Better validation loss {:.4f} found, save model'.format(valid_loss))



        if sum_index:
            best_model_s=valid_sum > best_sum
        else:
            best_model_s=False



        if best_model_s:
            filename_previous_s=filename_s
            filename_s = 'sum_epoch_' + str(train_epoch) + '_' + str(args.learning_rate) + '_loss_' + str(
                valid_loss) + '.pt'

            metric_filename_previous_s=metric_filename_s
            metric_filename_s=' bleu4= '+str(round(scores['Bleu_4'],4))+' METEOR= '+str(round(scores['METEOR'],4))+' ROUGE_L= '+str(round(scores['ROUGE_L'],4))+' CIDEr= '+str(round(scores['CIDEr'],4))+str(args.n_layers)+str(args.d_model)
            save_model(model,optimizer,filename_s+metric_filename_s,filename_previous_s+metric_filename_previous_s,train_epoch)

            best_sum=valid_sum
            best_filename_s=filename_s+metric_filename_s
            best_epoch_s=train_epoch
            print('*'*5)
            print('Better validation loss {:.4f} found, save model'.format(valid_loss))



        # learning rate decay
        scheduler.step(valid_loss)


        print('-'*80)
        print('Epoch {} summary'.format(train_epoch))
        print('Train loss: {:.4f}, val loss: {:.4f}, Time: {:.4f}s'.format(
            epoch_loss, valid_loss, time.time()-t_epoch_start
        ))

        print('-'*80)

    optimizer_to_cpu(optimizer)

    path = os.path.join('saved_models', best_filename)
    model.load_state_dict(torch.load(path)['state_dict'])
    filename = 'test_' + str(best_epoch) + '_'  + str(args.learning_rate) + '_loss_'+str(best_loss)+ '.pt'

    scores=test_evaluator.evaluate(utils, model, train_epoch, cfg,args,voc,beam_length,epoch_loss)
    metric_filename = ' bleu4= ' + str(round(scores['Bleu_4'], 4)) + ' METEOR= ' + str(
        round(scores['METEOR'], 4)) + ' ROUGE_L= ' + str(round(scores['ROUGE_L'], 4)) + ' CIDEr= ' + str(
        round(scores['CIDEr'], 4)) + str(args.n_layers) + str(args.d_model)
    save_model(model, optimizer, filename+metric_filename,'',0)

    print('test scores')
    print(scores)


    if sum_index:
        path_s = os.path.join('saved_models', best_filename_s)
        model.load_state_dict(torch.load(path_s)['state_dict'])
        filename_s = 'sum_test_' + str(best_epoch_s) + '_' + str(args.learning_rate) + '_loss_' + str(
            best_loss) + '.pt'

        scores_s = test_evaluator.evaluate(utils, model, train_epoch, cfg, args, voc, beam_length, epoch_loss)
        metric_filename_s = ' bleu4= ' + str(round(scores_s['Bleu_4'], 4)) + ' METEOR= ' + str(
            round(scores_s['METEOR'], 4)) + ' ROUGE_L= ' + str(round(scores_s['ROUGE_L'], 4)) + ' CIDEr= ' + str(
            round(scores_s['CIDEr'], 4)) + str(args.n_layers) + str(args.d_model)
        save_model(model, optimizer, filename_s + metric_filename_s,'',0)

        print('test scores sum')
        print(scores_s)


    print('test scores')
    print(scores)

    model.cpu()






### Training the network ###
def train(epoch, model, optimizer, train_loader_2d,train_loader_3d,  args,cfg):
    model.train() # training mode
    train_loss = []
    nbatches = len(train_loader_2d)
    t_iter_start = time.time()

    sample_prob = min(args.sample_prob, int(epoch/5)*0.05)


    if cfg.feats_3d==False :
        train_loader_3d=train_loader_2d








    for train_iter, (data_2d,data_3d) in enumerate(zip(train_loader_2d,train_loader_3d)):

        features_2d, targets, _, max_length, ides = data_2d






        if cfg.feats_3d==True:
            features_3d, _, _, _, _ = data_3d

        else:
            features_3d=0





        targets = targets.transpose(0, 1)




        so = torch.ones((cfg.batch_size, 1), dtype=int)

        trg_temp = torch.cat((so, targets), 1)





        img_batch_2d = Variable(features_2d)
        if cfg.feats_3d==True :
            img_batch_3d = Variable(features_3d)
        else:
            img_batch_3d = Variable(torch.Tensor(features_3d))

        sentence_batch = Variable(trg_temp)








        if args.cuda:
            img_batch_2d = img_batch_2d.cuda()
            img_batch_3d = img_batch_3d.cuda()

            sentence_batch = sentence_batch.cuda()

        t_model_start = time.time()


        (pred_score, gt_score) = model(img_batch_2d.float(),img_batch_3d.float(),   sentence_batch,cfg,
                                       sample_prob=sample_prob)





        sent_loss = F.cross_entropy(pred_score, gt_score,ignore_index=cfg.PAD_token)

        total_loss = sent_loss






        optimizer.zero_grad()
        total_loss.backward()

        # enable the clipping for zero mask loss training
        total_grad_norm = clip_grad_norm_(filter(lambda p: p.requires_grad, model.parameters()),
                                         args.grad_norm)

        optimizer.step()

        train_loss.append(total_loss.data.item())

        t_model_end = time.time()
        print('iter: [{}/{}], training loss: {:.4f}, '
              'sentence: {:.4f}, '
              'grad norm: {:.4f} '
              'data time: {:.4f}s, total time: {:.4f}s'.format(
            train_iter, nbatches, total_loss.data.item(), sent_loss.data.item(),
            total_grad_norm,
            t_model_start - t_iter_start,
            t_model_end - t_iter_start
        ), end='\r')

        t_iter_start = time.time()


    return np.mean(train_loss)


### Validation ##
def valid(model, loader_2d,loader_3d,cfg):
    model.eval()
    valid_loss = []



    if cfg.feats_3d==False:
        loader_3d=loader_2d




    #for iter, data_2d in enumerate(loader_2d):
    for iter, (data_2d,data_3d) in enumerate(zip(loader_2d,loader_3d)):


        features_2d, targets,_,_, _ = data_2d
        if cfg.feats_3d==True:
            features_3d, _, _, _, _ = data_3d
        else:
            features_3d=0





        targets = targets.transpose(0, 1)


        so = torch.ones((targets.size()[0], 1), dtype=int)



        #
        #
        #
        trg_temp = torch.cat((so, targets), 1)


        

        with torch.no_grad():
            img_batch_2d = Variable(features_2d)
            if cfg.feats_3d:
                img_batch_3d = Variable(features_3d)
            else:
                img_batch_3d = Variable(torch.Tensor(features_3d))

            sentence_batch = Variable(trg_temp)

            if args.cuda:
                img_batch_2d = img_batch_2d.cuda()
                img_batch_3d = img_batch_3d.cuda()

                sentence_batch = sentence_batch.cuda()


            (pred_score, gt_score) = model(img_batch_2d.float(),img_batch_3d.float(), sentence_batch,cfg)


            sent_loss = F.cross_entropy(pred_score, gt_score,ignore_index=cfg.PAD_token)

            total_loss = sent_loss




            valid_loss.append(total_loss.data.item())

    return (np.mean(valid_loss))





if __name__ == "__main__":


    batch_size=10


    ###sample probbility (probability to use generated words from the decoder instead of the GT during training###
    sample_prob=0

    ###beam length for beam decoding###
    beam_length=1

    ###number of transforme layers
    nlayers=9

    ####transformer arguments###
    dmod=1024 ##input size
    hidden_sz=2048##feed forward layers size

    ###gradient clip threshold
    grad_norm=0.75

    ###patience epochs before half lr
    patience_epochs=6

    ##training epochs
    epochs=100

    ###use 3d modality
    feats_3d=True


    ###a best model based on metrics sum and one on loss
    sum_index=True

    #random seed
    seed=2


    ###frames to keep from each video
    feat_len=50

    learning_rate=0.01


    ###multimodal

    ####different experiments presented on the paper
    #Ai
    #fusion='early'
    #attention='add'
    #downsample=False

    # # Aii
    #fusion = 'early'
    #attention='both_add'
    #downsample = False
    #
    # # Aiii
    #fusion = 'early'
    #attention='concat'
    #downsample = True
    #
    #
    # # Aiv
    #fusion = 'early'
    #attention='both'
    #downsample = True
    #
    #
    # # Bi
    #fusion = 'intermediate'
    #attention = 'add'
    #downsample = False
    #
    # # Bii
    #fusion = 'intermediate'
    #attention='both_add'
    #downsample = False
    #
    #
    # # Biii
    #fusion = 'intermediate'
    #attention='concat'
    #downsample = True
    #
    #
    # # Biv
    fusion = 'intermediate'
    attention='both'
    downsample=True


    ###load pretrained model
    finetune=False
    path_ft=''


    ###True if u want to evaluate an existing checkpoint
    evaluation=False
    path_ev=''








    main(args, batch_size, sample_prob, beam_length, nlayers, dmod, hidden_sz, grad_norm, patience_epochs, epochs,
         feats_3d, sum_index, seed, feat_len, learning_rate, fusion, attention, downsample, finetune, path_ft, evaluation,path_ev)












