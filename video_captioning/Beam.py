import torch
from Batch import nopeak_mask
import torch.nn.functional as F
import math
from torch.autograd import Variable
import numpy as np
import copy


def create_masks(trg, cfg):
    if trg is not None:
        trg_mask = (trg != cfg.PAD_token).unsqueeze(-2)
        size = trg.size(1)  # get seq_len for matrix
        np_mask = nopeak_mask(size)
        if trg.is_cuda:
            np_mask.cuda()
        trg_mask = trg_mask & np_mask

    else:
        trg_mask = None
    return trg_mask


def nopeak_mask(size):
    np_mask = np.triu(np.ones((1, size, size)),
                      k=1).astype('uint8')
    np_mask = Variable(torch.from_numpy(np_mask) == 0)
    np_mask = np_mask.cuda()
    return np_mask


def mask(out):
    out_mask = mask.unsqueeze(-1).expand_as(out)
    return out[out_mask].view(-1, out.size(-1))


def init_vars(src_2d,src_3d, trg, model, cfg, args, beam_length, max_len):
    init_tok = cfg.SOS_token
    # src_mask = (src != SRC.vocab.stoi['<pad>']).unsqueeze(-2)
    src_mask = None

    if model.fusion == 'early' and cfg.feats_3d==True:

        input_2d = model.emb_2d(src_2d.contiguous())
        input_3d = model.emb_3d(src_3d.contiguous())

        if model.attention == 'concat':
            input = torch.cat((input_2d, input_3d), 2)
        elif model.attention == 'add':
            input = input_2d + input_3d
        elif model.attention == 'both' or model.attention == 'both_add':
            h1 = model.selfattn_2d(input_3d, input_2d, input_2d, None)
            h2 = model.selfattn_3d(input_2d, input_3d, input_3d, None)

            if model.attention == 'both':
                input = torch.cat((h1, h2), 2)
            elif model.attention == 'both_add':
                input = h1 + h2
        elif model.attention == '2d':
            input = model.selfattn_2d(input_3d, input_2d, input_2d, None)
        elif model.attention == '3d':
            input = model.selfattn_3d(input_2d, input_3d, input_3d, None)

        if model.downsample:
            input = model.down(input)

    else:
        input = model.emb_2d(src_2d.contiguous())





    x_emb = model.emb_out(input)

    if model.fusion == 'intermediate' and cfg.feats_3d==True:
        input_3d = model.emb_3d(src_3d.contiguous())
        x_emb_3d = model.emb_out(input_3d)
        e_output_3d = model.cap_model.encoder_3d(x_emb_3d, src_mask)

    e_output = model.cap_model.encoder(x_emb, src_mask)

    if model.fusion == 'intermediate' and cfg.feats_3d==True:
        if model.attention == 'concat':
            encoding_f = []
            for i in range(len(e_output)):
                encoding_f.append(torch.cat((e_output[i], e_output_3d[i]), 2))
            e_output = encoding_f
        elif model.attention == 'add':
            e_output = e_output + e_output_3d
        elif model.attention == 'both' or model.attention == 'both_add':
            h1 = []
            h2 = []
            for i in range(len(e_output)):
                h1.append(model.cap_model.selfattn_2d(e_output_3d[i], e_output[i], e_output[i], None))
                h2.append(model.cap_model.selfattn_3d(e_output[i], e_output_3d[i], e_output[i], None))

            if model.attention == 'both':

                if model.attention == 'both':
                    encoding_f = []
                    for i in range(len(e_output)):
                        encoding_f.append(torch.cat((h1[i],h2[i]), 2))
                    e_output = encoding_f

            elif model.attention == 'both_add':
                e_output = h1 + h2

        elif model.attention == '2d':
            encoding_f = []
            for i in range(len(e_output)):
                encoding_f.append(model.cap_model.selfattn_2d(e_output_3d[i], e_output[i], e_output[i], None))
            e_output=encoding_f


        elif model.attention == '3d':
            encoding_f = []
            for i in range(len(e_output)):
                encoding_f.append(model.cap_model.selfattn_3d(e_output[i], e_output_3d[i], e_output_3d[i], None))
            e_output = encoding_f

        elif model.attention == 'tvm_add' or model.attention == 'tvm_att':
            encoding_f=[e_output,e_output_3d]

            e_output=encoding_f

        if model.attention == 'both' or model.attention == 'concat':
            temp = []
            for i in range(len(e_output)):
                temp.append(model.cap_model.fuse_out(e_output[i]))
            e_output = temp





    if model.fusion=='late' and cfg.feats_3d==True:
        input_3d = model.emb_3d(src_3d.contiguous())
        x_emb_3d = model.emb_out(input_3d)

        e_output_3d = model.cap_model_3d.encoder(x_emb_3d, None)

    else:
        e_output_3d=0


    new_y = torch.LongTensor([[init_tok]])
    if args.cuda:
        new_y = new_y.cuda()


    trg_mask = nopeak_mask(1)



    h = model.cap_model.decoder(new_y, e_output, trg_mask,model.out,model.attention)

    if model.fusion == 'late' and cfg.feats_3d==True:
        h_3d = model.cap_model_3d.decoder(new_y, e_output_3d, trg_mask,model.out,model.attention)


    if model.fusion == 'late' and cfg.feats_3d==True:
        if model.attention=='both':
            h1 = model.selfattn_2d(h_3d, h, h, trg_mask)
            h2 = model.selfattn_3d(h, h_3d, h_3d, trg_mask)
            h = torch.cat((h1, h2), 2)
            h = model.out1(h)

        if model.attention=='both_add':

            h1 = model.selfattn_2d(h_3d, h, h, trg_mask)
            h2 = model.selfattn_3d(h, h_3d, h_3d, trg_mask)
            h=h1+h2


        elif model.attention=='2d':
            h = model.selfattn_2d(h_3d, h, h, trg_mask)
        elif model.attention=='3d':
            h = model.selfattn_3d(h, h_3d, h_3d, trg_mask)
        elif model.attention == 'concat':
            h = torch.cat((h, h_3d), 2)
            h = model.out1(h)
        elif model.attention == 'add':
            h = h+h_3d

    out = model.out(h)



    out = F.log_softmax(out, dim=-1)
    #
    probs, ix = out[:, -1].data.topk(beam_length)
    log_scores=probs




    outputs = torch.zeros(beam_length, max_len).long()
    if args.cuda:
        outputs = outputs.cuda()
    outputs[:, 0] = init_tok
    outputs[:, 1] = ix[0]






    return outputs, e_output, e_output_3d, log_scores


def k_best_outputs(outputs, out, log_scores, i, k):
    probs, ix = out[:, -1].data.topk(k)


    log_probs = probs + log_scores.transpose(0,1)


    k_probs, k_ix = log_probs.view(-1).topk(k)


    row = k_ix // k
    col = k_ix % k




    outputs[:, :i] = outputs[row, :i]
    outputs[:, i] = ix[row, col]

    log_scores = k_probs.unsqueeze(0)

    return outputs, log_scores


def beam_search(src_2d,src_3d, trg, model, cfg, opt, max_len, voc, beam_length):

    outputs, e_outputs,e_outputs_3d, log_scores = init_vars(src_2d,src_3d, trg, model, cfg, opt, beam_length, max_len)

    eos_tok = cfg.EOS_token
    ind = None

    for i in range(2, max_len):

        beam_out = []

        trg_mask = nopeak_mask(i)

        for j in range(beam_length):


            h = model.cap_model.decoder(outputs[j, :i].unsqueeze(0), e_outputs, trg_mask,model.out,model.attention)


            if model.fusion == 'late' and cfg.feats_3d==True:

                h_3d = model.cap_model_3d.decoder(outputs[j, :i].unsqueeze(0), e_outputs_3d, trg_mask,model.out,model.attention)


                if model.attention == 'both':
                    h1 = model.selfattn_2d(h_3d, h, h, None)
                    h2 = model.selfattn_3d(h, h_3d, h_3d, None)
                    h = torch.cat((h1, h2), 2)
                    h = model.out1(h)

                elif model.attention == 'both_add':

                    h1 = model.selfattn_2d(h_3d, h, h, None)
                    h2 = model.selfattn_3d(h, h_3d, h_3d, None)
                    h=h1+h2

                elif model.attention == '2d':
                    h = model.selfattn_2d(h_3d, h, h, None)
                elif model.attention == '3d':
                    h = model.selfattn_3d(h, h_3d, h_3d, None)
                elif model.attention == 'concat':
                    h = torch.cat((h, h_3d), 2)
                    h = model.out1(h)
                elif model.attention == 'add':
                    h = h+h_3d


            out = model.out(h)



            out = F.log_softmax(out, dim=-1)


            if j == 0:
                beam_out = out
            else:
                beam_out = torch.cat((beam_out, out), 0)




        outputs, log_scores = k_best_outputs(outputs, beam_out, log_scores, i, beam_length)


        ones = (outputs == eos_tok).nonzero()  # Occurrences of end symbols for all input sentences.
        sentence_lengths = torch.zeros(len(outputs), dtype=torch.long).cuda()
        for vec in ones:
            i = vec[0]
            if sentence_lengths[i] == 0:  # First end symbol has not been found yet
                sentence_lengths[i] = vec[1]  # Position of first end symbol



        num_finished_sentences = len([s for s in sentence_lengths if s > 0])



        if num_finished_sentences == beam_length:
            alpha = 0.7

            div = 1 / (sentence_lengths.type_as(log_scores) ** alpha)


            _, ind = torch.max(log_scores * div, 1)
            ind = ind.data[0]



            break





    if ind is None:
        temp = (outputs[0] == eos_tok).nonzero()

        if len(temp) < 1:
            length = outputs.size()[1] - 1
        else:
            length = (outputs[0] == eos_tok).nonzero()[0]

        return ' '.join([voc.index2word[tok.item()] for tok in outputs[0][1:length]])

    else:


        temp = (outputs[ind] == eos_tok).nonzero()
        if len(temp) < 1:
            length = outputs.size()[1] - 1
        else:
            length = (outputs[ind] == eos_tok).nonzero()[0]


        return ' '.join([voc.index2word[tok.item()] for tok in outputs[ind][1:length]])







