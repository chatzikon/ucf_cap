# UCF_CAP_VIDEO_CAPTIONING_IN_THE_WILD

The github of the ICIP 2022 paper: UCF-CAP, VIDEO CAPTIONING IN THE WILD

# Requirements

The code executed at enviroment using:

python 3.6

torch 1.4.0

torchtext 0.5.0

torchvision 0.5.0

spacy 2.2.0

h5py 2.9.0

numpy

pandas

dill

nltk

 #  Train 

Use train.py for training a new model. The eight schemes employed in the paper are (and the corresponding arguments to use at main function):

| Fusion scheme| Fusion | Attention | Downsample |
| ------ | ------ |------|------|
| A-i | Early | add | False |
| A-ii| Early |both_add | False |
 | A-iii | Early | concat | True |
| A-iv | Early |both | True |
| B-i | Intermediate | add | False |
| B-ii | Intermediate |both_add | False |
| B-iii | Intermediate | concat | True |
| B-iv | Intermediate |both | True |

The late fusion concerns the after decoded fusion that didn't achieve good results and it wasn't employed at the paper.

The feature files (extracted 2d and 3d features) can be found [here](https://drive.google.com/drive/u/0/folders/12B_2gdOsT4SNC0iDMs8ed0fXgc2Omh9k) (resnet152 and c3d features) and should be placed at datasets/UCF_V2/features folder.

The default arguments are the ones employed at the paper

Checkpoints can be found [here](https://drive.google.com/drive/u/0/folders/1Q-fwmQfHl1HefdifeRHbc_rNCkbdYCHd)

The dataset link can be found at the paper.

# Finetune
Use train.py with finetune=True, path_ft=True and the corresponding checkpoint at video captioning/checkpoint folder 

# Evaluation
Use train.py with evaluation=True and path_ev=True and the corresponding checkpoint at video captioning/checkpoint folder 

# Acknowledgements 

Code parts of https://github.com/salesforce/densecap and https://github.com/nasib-ullah/video-captioning-models-in-Pytorch were employed during implementation. 


# License
Our code is released under MIT License (see LICENSE file for details)

